/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

import java.util.Date;

/**
 *
 * @author jordynwalker
 */
public class ButlerRobotFactory implements RobotFactoryInterface {

    @Override
    public Robot makeRobot(String name) {
	ButlerRobot robot = new ButlerRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.BUTLER);
	return robot;
    }

} 
    

