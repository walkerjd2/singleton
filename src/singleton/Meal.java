/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author jordynwalker
 */
public class Meal {
     private String mainCourse;
    private String drink;
    private String sideDish;
    private String dessert;

    public String getMainCourse() {
	return mainCourse;
    }

    public void setMainCourse(String mainCourse) {
	this.mainCourse = mainCourse;
    }

    public String getDrink() {
	return drink;
    }

    public void setDrink(String drink) {
	this.drink = drink;
    }

    public String getSideDish() {
	return sideDish;
    }

    public void setSideDish(String sideDish) {
	this.sideDish = sideDish;
    }

    public String getDessert() {
	return dessert;
    }

    public void setDessert(String dessert) {
	this.dessert = dessert;
    }

    public String toString() {
	return "For dinner, the main course will be " + mainCourse
		+ " served with a side of " + sideDish
		+ " paired with a nice glass of " + drink
		+ ". Dessert will be " + dessert + ". Yummy!";
    }
}
}
