/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

import java.util.Date;

/**
 *
 * @author jordynwalker
 */
public class MultiPurposeRobotFactory implements RobotFactoryInterface {

    @Override
    public Robot makeRobot(String name) {
	MultiPurposeRobot robot = new MultiPurposeRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.MULTIPURPOSE);

	return robot;
    }

}  

