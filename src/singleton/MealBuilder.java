/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author jordynwalker
 */
public class MealBuilder {
    protected Meal meal = new Meal();

    public abstract void buildMainCourse();

    public abstract void buildDrink();

    public abstract void buildDessert();

    public abstract void buildSideDish();

    public abstract Meal getMeal();
}

