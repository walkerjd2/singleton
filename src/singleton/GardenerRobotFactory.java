/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author jordynwalker
 */
public class GardenerRobotFactory implements RobotFactoryInterface {

    @Override
    public Robot makeRobot(String name) {
	GardenerRobot robot = new GardenerRobot();
	robot.setName(name);
	robot.setDateCreated(new Date());
	robot.setRole(Role.GARDENER);
	return robot;
    }
}
    
